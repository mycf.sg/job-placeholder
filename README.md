# `job-placeholder`

[![pipeline status](https://gitlab.com/mycf.sg/job-placeholder/badges/master/pipeline.svg)](https://gitlab.com/mycf.sg/job-placeholder/-/commits/master)


This repository contains a placeholder service used during cluster migrations to ensure configurations are in place before replacing the image.

The Docker image can be found at [https://hub.docker.com/r/mycfsg/job-placeholder](https://hub.docker.com/r/mycfsg/job-placeholder).

# Configuration

None

# Deployment

## Docker

To pull in the image:

```sh
docker pull mycfsg/job-placeholder:latest;
```

To run a container based on this image:

```sh
docker run -it mycfsg/job-placeholder:latest;
```

## Kubernetes

The following is a Kuberentes manifest that works:

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: placeholder
  namespace: mcf
spec:
  # ref https://crontab.guru
  schedule: "0 */2 * * *"
  # keep history for 2 days
  successfulJobsHistoryLimit: 24
  failedJobsHistoryLimit: 24
  jobTemplate:
    spec:
      backoffLimit: 5
      parallelism: 1
      template:
        metadata:
          name: placeholder
        spec:
          restartPolicy: Never
          imagePullSecrets:
          - name: docker-secret
          containers:
          - name: placeholder
            image: mycfsg/job-placeholder:latest
            imagePullPolicy: IfNotPresent
            resources:
              limits:
                cpu: 550m
                memory: 550Mi
              requests:
                cpu: 500m
                memory: 500Mi

```

- You should be able to run `kubectl get pods | grep placeholder` to get the pod name once the above YAML is applied

# Usage

On trigger, the container will dump a pretty-formatted JSON to `stdout`. For example given a specification where the `entrypoint` is set to `sleep` and the argument passed in is `5`, the following will be dumped before exiting with status code 0.

```json
{
  "command": "sleep",
  "arguments": [
    "5"
  ],
  "hostname": "0180a0d564d0",
  "timestamp": "2020-06-01T07:57:04.037527177Z",
  "operating_system": "linux",
  "architecture": "amd64",
  "environment": {
    "   ": "/job-placeholder_linux_amd64",
    "HOME": "/root",
    "HOSTNAME": "0180a0d564d0",
    "PATH": "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "TERM": "xterm"
  },
  "network_interfaces": [
    "ip+net/127.0.0.1/8",
    "ip+net/172.17.0.2/16"
  ]
}
```

# Development Runbook

## Getting Started

1. Clone this repository
2. Run `make deps` to pull in external dependencies
3. Write some awesome stuff
4. Run `make test` to ensure unit tests are passing
5. Run `make build` to ensure the binary is buildable
6. Run `make image` to ensure the binary is production-compilable
7. Push to remote

## Continuous Integration (CI) Pipeline

### On Gitlab

Gitlab is used to run tests and ensure that builds run correctly.

#### Version Bumping

1. Run `make .ssh`
2. Copy the contents of the file generated at `./.ssh/id_rsa.base64` into an environment variable named **`DEPLOY_KEY`** in **Settings > CI/CD > Variables**
3. Navigate to the **Deploy Keys** section of the **Settings > Repository > Deploy Keys** and paste in the contents of the file generated at `./.ssh/id_rsa.pub` with the **Write access allowed** checkbox enabled

- **`DEPLOY_KEY`**: generate this by running `make .ssh` and copying the contents of the file generated at `./.ssh/id_rsa.base64`

#### DockerHub Publishing

1. Login to [https://hub.docker.com](https://hub.docker.com), or if you're using your own private one, log into yours
2. Navigate to [your security settings at the `/settings/security` endpoint](https://hub.docker.com/settings/security)
3. Click on **Create Access Token**, type in a name for the new token, and click on **Create**
4. Copy the generated token that will be displayed on the screen
5. Enter the following varialbes into the CI/CD Variables page at **Settings > CI/CD > Variables** in your Gitlab repository:

- **`DOCKER_REGISTRY_URL`**: The hostname of the Docker registry (defaults to `docker.io` if not specified)
- **`DOCKER_REGISTRY_USERNAME`**: The username you used to login to the Docker registry
- **`DOCKER_REGISTRY_PASSWORD`**: The generated access token

# License

Code in this repository is licensed under the [MIT License](./LICENSE)
