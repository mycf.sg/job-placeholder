package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"runtime"
	"strings"
	"time"
)

type SystemInfo struct {
	Command           string            `json:"command"`
	Arguments         []string          `json:"arguments"`
	Hostname          string            `json:"hostname"`
	Timestamp         time.Time         `json:"timestamp"`
	OperatingSystem   string            `json:"operating_system"`
	Architecture      string            `json:"architecture"`
	Environment       map[string]string `json:"environment"`
	NetworkInterfaces []string          `json:"network_interfaces"`
}

func main() {
	systemInfo := SystemInfo{
		Environment: map[string]string{},
	}
	hostname, _ := os.Hostname()
	systemInfo.Hostname = hostname
	systemInfo.Timestamp = time.Now()
	environment := os.Environ()
	systemInfo.OperatingSystem = runtime.GOOS
	systemInfo.Architecture = runtime.GOARCH
	interfaceAddrs, _ := net.InterfaceAddrs()
	for _, interfaceAddr := range interfaceAddrs {
		systemInfo.NetworkInterfaces = append(systemInfo.NetworkInterfaces, fmt.Sprintf("%s/%s", interfaceAddr.Network(), interfaceAddr.String()))
	}
	for _, env := range environment {
		split := strings.SplitN(env, "=", 2)
		key := split[0]
		value := ""
		if len(split) > 1 {
			value = split[1]
		}
		systemInfo.Environment[key] = value
	}
	args := os.Args
	systemInfo.Command = args[0]
	systemInfo.Arguments = args[1:]

	systemInfoJSON, marshalError := json.MarshalIndent(systemInfo, "", "  ")
	if marshalError != nil {
		log.Fatalf("failed to marshal json '%v': %s", systemInfo, marshalError)
	}
	fmt.Printf("%s", systemInfoJSON)
}
